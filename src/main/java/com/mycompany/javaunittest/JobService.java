package com.mycompany.javaunittest;

import java.time.LocalDate;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nutcha1223
 */
public class JobService {

    public static boolean checkEnableTime(LocalDate startTime, LocalDate endTime, LocalDate today) {
        if (today.isBefore(startTime)) {
            return false;
        }
        if (today.isAfter(endTime)) {
            return false;
        }
        return true;
    }
}
